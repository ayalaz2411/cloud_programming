#!/bin/bash

# Debug
set -o xtrace

# Create a unique key pair for the instance
CURRENT_DATE=$(date +"%Y-%m-%d-%H:%M:%S")
KEY_NAME="cloud-course-$CURRENT_DATE"
KEY_PEM="$KEY_NAME.pem"

echo "Creating key pair $KEY_PEM to connect to instances and saving locally"
aws ec2 create-key-pair --key-name $KEY_NAME | jq -r ".KeyMaterial" > $KEY_PEM

# Secure the key pair
chmod 400 $KEY_PEM

# Create a security group for the instance
SEC_GRP="my-sg-$CURRENT_DATE"

echo "Creating security group $SEC_GRP"
aws ec2 create-security-group --group-name $SEC_GRP --description "Access my instances"

# Add SSH and HTTP access to the security group
echo "Adding rules to security group $SEC_GRP"
aws ec2 authorize-security-group-ingress --group-name $SEC_GRP --port 22 --protocol tcp --cidr 0.0.0.0/0
aws ec2 authorize-security-group-ingress --group-name $SEC_GRP --port 5000 --protocol tcp --cidr 0.0.0.0/0

# Launch an Ubuntu 20.04 instance
UBUNTU_20_04_AMI="ami-042e8287309f5df03"

echo "Creating Ubuntu 20.04 instance..."
RUN_INSTANCES=$(aws ec2 run-instances --image-id $UBUNTU_20_04_AMI --instance-type t2.micro --key-name $KEY_NAME --security-groups $SEC_GRP)

INSTANCE_ID=$(echo $RUN_INSTANCES | jq -r '.Instances[0].InstanceId')

echo "Waiting for instance $INSTANCE_ID to run..."
aws ec2 wait instance-running --instance-ids $INSTANCE_ID

# Get the public IP address of the instance
PUBLIC_IP=$(aws ec2 describe-instances --instance-ids $INSTANCE_ID | jq -r '.Reservations[0].Instances[0].PublicIpAddress')

echo "New instance $INSTANCE_ID @ $PUBLIC_IP"

# Deploy the Flask application to the instance
echo "Deploying code to production..."
scp -i $KEY_PEM -o "StrictHostKeyChecking=no" -o "ConnectionAttempts=60" app.py ubuntu@$PUBLIC_IP:/home/ubuntu/

# Install necessary packages and run the Flask application
echo "Setting up production environment..."
ssh -i $KEY_PEM -o "StrictHostKeyChecking=no" -o "ConnectionAttempts=10" ubuntu@$PUBLIC_IP <<EOF
    sudo apt update
    sudo apt install python3-pip -y
    sudo pip3 install Flask
    Run app
    nohup flask run --host 0.0.0.0 &>/dev/null &
    exit
EOF

# Set the INSTANCE_ID environment variable for future use
export INSTANCE_ID=$INSTANCE_ID

# Test that the Flask application is working properly
echo "Testing that the Flask application is working properly..."
curl  --retry-connrefused --retry 10 --retry-delay 1 --request POST "http://$PUBLIC_IP:5000/entry?plate=123-123-123&parkingLot=382" 
sleep 20
curl  --retry-connrefused --retry 10 --retry-delay 1 --request POST "http://$PUBLIC_IP:5000/exit?ticketId=0"
