from datetime import datetime
from flask import Flask, request, jsonify

app = Flask(_name_)

entries = {}
tickets = {}
ticket = 0


@app.route("/entry", methods=["POST"])
def entry():
    global ticket
    parking_lot_id = request.args.get('parkingLot', None)
    plate_number = request.args.get('plate', None)

    if plate_number:
        if plate_number in entries:
            return jsonify({"error": "Plate number already parking"})
        else:
            entry_time = datetime.now().isoformat()
            entries[plate_number] = {
                "ticket_id": ticket,
                "plate": plate_number,
                "parking_lot": parking_lot_id,
                "entry_time": datetime.now().isoformat(),
            }
            tickets[ticket] = plate_number
            ticket += 1
            return jsonify({"ticket_id": ticket - 1, "plate": plate_number, "parking_lot": parking_lot_id, "entry_time": entry_time})
    else:
        return jsonify({"error": "Plate number not found"})


@app.route("/exit", methods=["POST"])
def exit():
    ticket_id = request.args.get("ticketId")
    try:
        ticket_id = int(ticket_id)
    except Exception:
        return jsonify({"error": "Ticket not found"})
    if ticket_id in tickets:
        plate_number = tickets[ticket_id]
        parking_instance = entries[plate_number]
        parking_lot = parking_instance["parking_lot"]

        entry_time = datetime.fromisoformat(parking_instance["entry_time"])
        exit_time = datetime.now()
        parking_time = (exit_time - entry_time).total_seconds() // 60

        parking_fee = parking_time // 15 * 2.5
        del entries[plate_number]
        del tickets[ticket_id]
        return jsonify({"ticket_id": ticket_id, "plate_number": plate_number, "parking lot": parking_lot, "parking duration": f'{parking_time} minutes', "parking fee": parking_fee})
    else:
        return jsonify({"error": "Ticket not found"})


if _name_ == '_main_':
    app.run(host="0.0.0.0", port=5000)
